>>> from lyrics import parse_lyrics
>>> lines = """Title
... Authors authors authors
... Copyright: blah
... 
... vERse 1:
... lines
... 
... Verse 2
... moar lines
... 
... Verse 3
...
... (Verse 1)
... (verse 1)""".split('\n')
>>> lyrics = parse_lyrics(lines)
>>> info = lyrics.next()
>>> print sorted(info.items())
[('authors', 'Authors authors authors'), ('copyright', 'blah'), ('title', 'Title')]
>>> list(lyrics)
['lines', 'Verse 2\nmoar lines', 'Verse 3', 'lines', 'lines']
