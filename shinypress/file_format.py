# coding: utf-8
from __future__ import absolute_import, print_function

import traceback
import codecs
import os

from .handlers import handlers
from .utils import warning

blank_handler = handlers['blank']


def read_presentation(filename, recursed=False):
    lines = codecs.open(filename, 'rU', 'utf-8')

    errors = False

    # FIXME: hack to ensure relative paths work
    old = os.getcwd()
    new = os.path.dirname(filename)
    if new:
        os.chdir(new)

    content = []
    extras = []
    handled_first = False

    for i, line in enumerate(lines):
        line = line.strip()
        if not line or line.startswith('#') or line.startswith('//'):
            continue

        handler, filename = line.split(':', 1)
        handler = handler.strip()
        filename = filename.strip()

        # Ensure that a blank slide appears between every set of content
        # jinja >=2.9 prevents this from working inside the template
        if handler != 'background':
            if handled_first:
                c, e = blank_handler(None)
                content.append((c, 'blank'))
                extras.extend(e)
            else:
                handled_first = True

        # handle included files
        # FIXME: no recursion detection! :)
        if handler == 'include':
            c, ex, er = read_presentation(filename, recursed=True)
            content.extend(c)
            extras.extend(ex)
            errors |= er
            continue

        if handler not in handlers:
            warning(i, 'Undefined handler: %s' % handler)
            errors = True
            continue

        handler_func = handlers[handler]
        try:
            c, e = handler_func(filename)
        except Exception:
            tb = traceback.format_exc()
            warning(i, 'Caught exception, traceback follows:\n%s' % (tb,))
            errors = True
        else:
            content.append((c, handler))
            extras.extend(e)

    if not recursed:
        # Always add a blank slide at the end if we're the outer one
        c, e = blank_handler(None)
        content.append((c, 'blank'))
        extras.extend(e)

    os.chdir(old)
    return content, extras, errors
