# coding: utf-8
from __future__ import absolute_import


def replace_all(s, o, n):
    while o in s:
        s = s.replace(o, n)
    return s


def latexify(content):
    """
    Turn Pango markup into latex code.
    Warning: does not check validity of markup!
    Warning: this also allows LaTeX code straight through
        (this is an intentional bug?)
    """
    replacements = {
        '#': r'\#',
        '_': r'\_',
        '$': r'\$',

        '<sc>': r'\textsc{',
        '</sc>': '}',

        '<sup>': r'\textsuperscript{',
        '</sup>': '}',

        '<tt>': r'\texttt{',
        '</tt>': '}',

        '<i>': r'\emph{',
        '</i>': '}',

        '<u>': r'\uline{',
        '</u>': '}',

        '<b>': r'\textbf{',
        '</b>': '}',

        '<resp>': r'\textbf{\emph{',
        '</resp>': '}}',

        '<ul>': r'\begin{itemize}',
        '</ul>': r'\end{itemize}',
        '<ol>': r'\begin{enumerate}',
        '</ol>': r'\end{enumerate}',
        '<li>': r'\item ',
        '</li>': ' ',
    }

    # replace all line breaks with LaTeX line breaks
    content = content.replace('\n', r'\\')
    content = content.replace('<br>', r'\\')

    # replace paragraphs with LaTeX paragraph breaks
    content = content.replace(r'<p>', '\n\n')

    # remove line breaks next to paragraph breaks
    content = replace_all(content, r'\\''\n', '\n')
    content = replace_all(content, '\n'r'\\', '\n')

    # prettify line breaks
    content = content.replace(r'\\', r'\\''\n')

    for old, new in replacements.items():
        content = content.replace(old, new)

    return content
