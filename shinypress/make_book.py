from __future__ import division, print_function

import argparse
import csv
import os
from os.path import join, abspath, expanduser
import sys

from PIL import Image

MAX_HEIGHT = 768
BUILD_DIR = 'build'
SRC_DIR = 'src'
SERIES_FILENAME = 'series'


def tile(images, output):
    images = map(Image.open, images)

    max_height = max(im.size[1] for im in images)
    max_height = min(max_height, MAX_HEIGHT)

    total_width = 0
    for i, im in enumerate(images):
        width, height = im.size
        aspect = width / height
        new_size = (int(max_height * aspect), max_height)
        im = im.resize(new_size, Image.BICUBIC)
        total_width += im.size[0]
        images[i] = im

    joined = Image.new('RGB', (total_width, max_height))
    x = 0
    for im in images:
        joined.paste(im, (x, 0))
        x += im.size[0]

    joined.save(output)


def main(args=None):
    if args is None:
        args = sys.argv[1:]
    parser = argparse.ArgumentParser(
        description="make-book utility"
    )
    parser.add_argument("workdir", nargs='?', default='.')
    pargs = vars(parser.parse_args(args))

    workdir = abspath(expanduser(pargs["workdir"]))
    builddir = join(workdir, BUILD_DIR)
    srcdir = join(workdir, SRC_DIR)

    try:
        os.mkdir(builddir)
    except OSError:
        print(builddir, "already exists")

    with open(join(srcdir, SERIES_FILENAME), 'rU') as series_file:
        for i, line in enumerate(csv.reader(series_file)):
            if not line:
                continue

            line = [join(srcdir, filename) for filename in line]

            output = join(builddir, '%.2d.jpg' % i)

            tile(line, output)


if __name__ == '__main__':
    main()
