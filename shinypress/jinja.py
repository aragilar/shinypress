# coding: utf-8
from __future__ import absolute_import

from jinja2 import Environment, PackageLoader

from .pango_markup import latexify

env = Environment(
    loader=PackageLoader(__name__, '.'),
    block_start_string='%%',
    block_end_string='%%',
    variable_start_string='<',
    variable_end_string='>',
    comment_start_string='###',
    comment_end_string='###',
    trim_blocks=True,
)
get_template = env.get_template

# load filters
env.filters['latexify'] = latexify
