# coding: utf-8
from __future__ import absolute_import, print_function

import itertools
from six import next


def group_paragraphs(lines):
    """
    Group an iterable by groups of non-empty lines.

    Warning: the iterables this returns must be used in order,
    otherwise badness will ensue.
    """

    # number of the current paragraph
    i = [0]
    up_to = lambda j: i[0] == j
    finished = lambda: up_to(-1)

    def next_paragraph():
        i[0] += 1

    def finish():
        i[0] = -1

    def make_iter(j):
        while True:
            if finished():
                break

            if not up_to(j):
                raise ValueError("iterators must be used in order")

            try:
                line = next(lines)
            except StopIteration:
                finish()
                return

            if not line:
                break

            yield line

        next_paragraph()

    for j in itertools.count():
        yield make_iter(j)
        if finished():
            break


def warning(line_no, msg):
    print('Warning (line %s): %s' % (line_no, msg))
