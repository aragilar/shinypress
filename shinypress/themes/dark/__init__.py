# coding: utf-8
from __future__ import absolute_import, print_function

from pkg_resources import resource_stream

from ..base import Theme


class DarkTheme(Theme):
    def __init__(self):
        super(DarkTheme, self).__init__(
            template='themes/dark/template.tex_t',
            extras=[
                resource_stream(__name__, name)
                for name in ('beamerthemedark.sty', 'tango.sty')
            ],
        )


theme = DarkTheme
