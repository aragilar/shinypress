# coding: utf-8
from __future__ import absolute_import, print_function

from ..jinja import get_template


class BaseTheme(object):
    pass


class Theme(BaseTheme):
    def __init__(self, template, extras=None):
        super(Theme, self).__init__()
        if extras is None:
            extras = ()
        self.template = get_template(template)
        self.extras = extras

    def render(self, content):
        source = self.template.render(content=content)
        return (source, self.extras)
