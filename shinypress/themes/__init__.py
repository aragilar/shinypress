# coding: utf-8
from __future__ import absolute_import, print_function

from .dark import theme as dark_theme

themes = dict(
    dark=dark_theme,
)
