# coding: utf-8
from __future__ import absolute_import, print_function

from os.path import basename
from random import choice
from string import ascii_lowercase
from io import BytesIO

from ..jinja import env


def selection(seq, n):
    return (choice(seq) for i in range(n))


template = env.from_string(r"""
%% if not fullsize %%
%% for page in range(1, n_pages+1) %%
\begin{frame}<frameopts>
\includegraphics[width=\linewidth,page=<page>]{<filename>}
\end{frame}
%% endfor %%
%% else %%
{
\backgroundoff
%% for page in range(1, n_pages+1) %%
\usebackgroundtemplate{\includegraphics[width=\paperwidth,page=<page>]{<filename>}}
\frame[plain]{}
%% endfor %%
}
%% endif %%
""")


def render(spec):
    part, spec = spec.split(' ', 1)

    frameopts = ''
    fullsize = False

    if part == '[plain]':
        frameopts = '[plain]'
        part, spec = spec.split(' ', 1)

    if part == '[fullsize]':
        fullsize = True
        part, spec = spec.split(' ', 1)

    n_pages = int(part)
    filename = spec

    content = open(filename, 'rb').read()
    f = BytesIO(content)
    f.name = ''.join(selection(ascii_lowercase, 12)) + '.pdf'
    filename = basename(f.name)
    return (
        template.render(
            frameopts=frameopts,
            fullsize=fullsize,
            filename=filename,
            n_pages=n_pages,
        ),
        [f],
    )
