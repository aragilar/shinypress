# coding: utf-8
from __future__ import absolute_import, print_function

import codecs

from ...jinja import get_template
from .parser import parse_lyrics

template = get_template('handlers/lyrics/template.tex')


def render(filename):
    source = codecs.open(filename, 'rU', encoding='utf-8')
    info, verses = parse_lyrics(source)
    return (template.render(info=info, verses=verses), [])
