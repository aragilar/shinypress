# coding: utf-8
from __future__ import absolute_import, print_function

import codecs

from six import next

from ..jinja import env
from ..utils import group_paragraphs

template = env.from_string(r"""
%% for para in text %%
\begin{frame}
%% if loop.first and title %%
\frametitle{< title | latexify >}
%% endif %%

< para | latexify >
\end{frame}
%% endfor %%
""")


def render(filename):
    source = codecs.open(filename, 'rU', 'utf-8')
    source = (line[:-1] for line in source)
    paragraphs = ('\n'.join(para) for para in group_paragraphs(source))
    title = next(paragraphs)
    return (template.render(title=title, text=paragraphs), [])


def title(title):
    return (r'\frame[plain]{\frametitle{%s}}' % title, [])
