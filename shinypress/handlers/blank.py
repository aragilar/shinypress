# coding: utf-8
from __future__ import absolute_import, print_function

BLANK_SLIDE = r"\frame[plain]{}"


def render(*args, **kwargs):
    return (BLANK_SLIDE, [])
