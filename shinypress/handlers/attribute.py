# coding: utf-8
from __future__ import absolute_import, print_function

import codecs

from ..jinja import env

template = env.from_string(u"""
\\begin{frame}
\\footnotesize
%% for attrib in attributions %%
“< attrib.title | latexify >”
(\\url{< attrib.author | latexify >})
< attrib.licence >
%% endfor %%
\\vfill\\vfill\\vfill\\vfill

\\end{frame}
""")

licence_str = {
    "CC BY 2.0 (http://creativecommons.org/licenses/by/2.0/)":
    "Creative Commons Attribution 2.0",
    "CC BY-NC-SA 2.0 (http://creativecommons.org/licenses/by-nc-sa/2.0/)":
    u"Creative Commons Attribution–NonCommercial–ShareAlike 2.0",
    "CC BY-SA 2.0 (http://creativecommons.org/licenses/by-sa/2.0/)":
    u"Creative Commons Attribution–ShareAlike 2.0 Generic",
    "CC BY-NC 2.0 (http://creativecommons.org/licenses/by-nc/2.0/)":
    u"Creative Commons Attribution–NonCommercial 2.0 Generic"
}


def split_attribution(text):
    """Splits attribution generated from http://openattribute.com"""
    attr = {}
    author_licence_split_str = " / "
    title_author_split_str = "(http"

    title_author, first_split, licence = text.partition(
        author_licence_split_str)
    title, second_split, author = title_author.partition(
        title_author_split_str)

    attr['licence'] = licence_str[licence.strip()]
    attr['title'] = title.strip()
    attr['author'] = (second_split + author).strip()[1:-2]  # removes parens

    return attr


def render(filename):
    source = codecs.open(filename, 'rU', 'utf-8')
    formatted_source = (split_attribution(line) for line in source)
    return (template.render(attributions=formatted_source), [])
