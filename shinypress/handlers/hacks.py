# coding: utf-8
from __future__ import absolute_import, print_function

import os.path


def background(filename):
    if filename:
        f = open(filename, 'rb')
        filename = os.path.basename(f.name)
        source = r'\setbackground{%s}' % (filename,)
        return (source, [f])
    return (r'\setbackground{}', [])
