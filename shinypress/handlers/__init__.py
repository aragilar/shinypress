# coding: utf-8
from __future__ import absolute_import, print_function

from . import text, lyrics, pdf, images, hacks, attribute, blank

handlers = dict(
    text=text.render,
    title=text.title,
    lyrics=lyrics.render,
    pdf=pdf.render,
    images=images.render,
    image=images.render_single,
    attributions=attribute.render,

    background=hacks.background,

    blank=blank.render,
)
