# coding: utf-8
from __future__ import absolute_import, print_function

from os import path
from glob import glob

from ..jinja import env

template = env.from_string(r"""
%% for filename in images %%
\begin{frame}<frameopts>
\begin{list}{}{%
\setlength{\leftmargin}{-1.5em}%
\setlength{\rightmargin}{-1.5em}%
}%
\item[]{\begin{center}
\includegraphics[
  width=\linewidth,height=\textheight - 1.25em,keepaspectratio
]{<filename>}
\end{center}}
\end{list}
\end{frame}
%% endfor %%
""")


def render(directory):
    frameopts = ''

    PLAIN = '[plain] '
    if directory.startswith(PLAIN):
        directory = directory[len(PLAIN):]
        frameopts = '[plain]'

    directory = path.abspath(directory)
    images = []
    for pattern in ('*.jpg', '*.png'):
        images.extend(
            glob(path.join(directory, pattern))
        )
    images.sort()
    return (template.render(images=images, frameopts=frameopts), [])


def render_single(image):
    return (template.render(images=[image], frameopts=''), [open(image, 'rb')])
