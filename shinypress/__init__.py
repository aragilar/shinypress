# coding: utf-8
from __future__ import absolute_import, print_function

import argparse
import sys

from .file_format import read_presentation
from .latex import render_pdf
from .themes import themes

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions

THEME = themes['dark']


def create_pdf(filename, theme, clean=True):
    content, extras, errors = read_presentation(filename)
    source, theme_extras = theme.render(content)
    extras.extend(theme_extras)

    try:
        return render_pdf(source, extras, clean)
    except RuntimeError:
        # LaTeX failed to run.
        if not bool(errors):
            raise
        print('Failed to build presentation.')


def shinypress(filename, clean, outfile=None):
    output = create_pdf(filename, THEME(), clean)
    if outfile is None:
        outfile = filename + '.pdf'
    if output is not None:
        with open(outfile, 'wb') as f:
            f.write(output)


def main(args=None):
    if args is None:
        args = sys.argv[1:]
    parser = argparse.ArgumentParser(
        description="shinypress presentation creator"
    )
    parser.add_argument("files", nargs='+')
    parser.add_argument("--no-clean", action='store_true', default=False)
    pargs = parser.parse_args(args)

    for filename in pargs.files:
        shinypress(filename, clean=not pargs.no_clean)
