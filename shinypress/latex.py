# coding: utf-8
from __future__ import absolute_import, print_function

import os.path
import tempfile
import shutil
from subprocess import Popen, PIPE
from codecs import open as codecs_open

HERE = os.path.abspath(os.path.dirname(__file__))

LATEX = 'xelatex'
LATEX_ARGS = []
FILENAME = 'source.tex'
OUTPUT_FILE = 'source.pdf'
ERROR_FORMAT = 'Failed to run LaTeX. STDOUT:\n{}\n---\nSTDERR:\n{}'


def render_pdf(source, extras, clean):
    path = tempfile.mkdtemp()

    # create the source file
    with codecs_open(
        os.path.join(path, FILENAME), 'w', encoding='utf-8'
    ) as f:
        f.write(source)

    for extra_file in extras:
        filename = os.path.basename(extra_file.name)
        with open(
            os.path.join(path, filename), "wb"
        ) as new_file:
            new_file.write(extra_file.read())

    p = Popen(
        [LATEX] + LATEX_ARGS + [FILENAME],
        cwd=path,
        stdin=PIPE, stdout=PIPE, stderr=PIPE,
        universal_newlines=True,
    )
    stdout, stderr = p.communicate('')

    if p.returncode != 0:
        raise RuntimeError(
            ERROR_FORMAT.format(
                stdout, stderr
            ),
        )

    filename = os.path.join(path, OUTPUT_FILE)
    with open(filename, 'rb') as f:
        data = f.read()
    if clean:
        shutil.rmtree(path, True)
    else:
        print(path)

    return data
