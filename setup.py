from setuptools import setup, find_packages

import versioneer

setup(
    name='ShinyPress',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='Generate PDF slides from a lyrics text file.',
    author='Peter Ward',
    author_email='peteraward@gmail.com',
    url="https://bitbucket.org/flowblok/shinypress/",
    install_requires=['jinja2', 'six', 'pillow'],
    packages=find_packages(),
    include_package_data=True,
    entry_points = {
        'console_scripts': [
            'shinypress = shinypress:main',
            'make-book = shinypress.make_book:main',
        ],
    },
)
